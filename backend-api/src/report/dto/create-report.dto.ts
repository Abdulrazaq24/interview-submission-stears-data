import { IsInt, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateReportDTO {
    @IsString()
    @ApiProperty({ example: 'Stears Data 2019 Report', description: 'Title of the Company Report' })
    readonly title: string;

    @IsString()
    @ApiProperty({ example: 'Q1', description: 'Yearly quarter' })
    readonly period: string;

    @IsInt()
    @ApiProperty({ example: '2020', description: 'Year of Report' })
    readonly year: number;

    @IsString()
    @ApiProperty({ example: 'Abdulrazaq Imam', description: 'Person assigned to submit the Report' })
    readonly assignee: string;

    @IsString()
    @ApiProperty({ example: '20th May', description: 'Report submission deadline' })
    readonly deadline: string;

    @IsString()
    @ApiProperty({ example: '5eb5556a0d1754080d7c051b', description: 'Referencing Company ID' })
    readonly postId: string;

    @IsString()
    @ApiProperty({ example: '5eb5556a0d1754080d7c051b', description: 'Referencing Company ID' })
    readonly submitted: {type: boolean, default: true};

    @IsString()
    @ApiProperty({ example: 'Reports goes as follows...', description: 'Report content' })
    readonly content: string;

    @ApiProperty()
    @ApiProperty({ example: '05/05/2020', description: 'Date Report created' })
    readonly createdAt: Date | string;
}