import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Param,
  NotFoundException,
  Post,
  Body,
  Put,
  Query,
  Delete,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ReportService } from './report.service';
import { CreateReportDTO } from './dto/create-report.dto';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';


@ApiBearerAuth()
@ApiTags('report')
@Controller('report')
export class ReportController {
  constructor(private reportService: ReportService) { }

  // Fetch all reports
  @Get('reports')
  @ApiResponse({
    status: 200,
    description: 'The found records',
    type: CreateReportDTO,
  })
  async getReports(@Res() res) {
    const reports = await this.reportService.getReports();
    return res.status(HttpStatus.OK).json(reports);
  }

  // Fetch a particular reports using ID
  @Get('report/:reportID')
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: CreateReportDTO,
  })
  async getReport(@Res() res, @Param('reportID', new ValidateObjectId()) reportID) {
    const report = await this.reportService.getReport(reportID);
    if (!report) {
        throw new NotFoundException('Report does not exist!');
    }
    return res.status(HttpStatus.OK).json(report);
  }

  // Submit a report
  @Post('/report')
  @ApiOperation({ summary: 'Submit a report' })
  @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async addReport(@Res() res, @Body() createReportDTO: CreateReportDTO) {
    const newReport = await this.reportService.addReport(createReportDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Report has been submitted successfully!',
      report: newReport,
    });
  }

  @Put('/edit')
  @ApiResponse({
    status: 200,
    description: 'Resource updated successfully'
  })
  async editReport(
    @Res() res,
    @Query('reportID', new ValidateObjectId()) reportID,
    @Body() createReportDTO: CreateReportDTO,
  ) {
    const editedReport = await this.reportService.editReport(reportID, createReportDTO);
    if (!editedReport) {
        throw new NotFoundException('Report does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Report has been successfully updated',
      post: editedReport,
    })
  }

  // Delete a report using ID
  @Delete('/delete')
  @ApiResponse({
    status: 200,
    description: 'Resource deleted successfully'
  })
  async deleteReport(@Res() res, @Query('reportID', new ValidateObjectId()) reportID) {
    const deletedReport = await this.reportService.deleteReport(reportID);
    if (!deletedReport) {
        throw new NotFoundException('Report does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Report has been deleted!',
      report: deletedReport,
    });
  }
}
