import { Document } from 'mongoose';

export interface Report extends Document {
  readonly title: string;
  readonly period: string;
  readonly year: number;
  readonly assignee: string;
  readonly deadline: string;
  readonly postId: string;
  readonly submitted: {type: boolean, default: true};
  readonly content: string;
  readonly createdAt: Date | string;
}