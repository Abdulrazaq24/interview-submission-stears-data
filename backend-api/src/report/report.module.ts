import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { ReportService } from './report.service';
import { ReportController } from './report.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ReportSchema } from './schemas/report.schema';
import { AuthenticationMiddleware } from '../common/authentication.middleware';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Report', schema: ReportSchema }]),
    ],
  providers: [ReportService],
  controllers: [ReportController]
})
export class ReportModule implements NestModule {
    configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
      consumer.apply(AuthenticationMiddleware).forRoutes(
        { method: RequestMethod.POST, path: '/report/post' },
        { method: RequestMethod.PUT, path: '/report/edit' },
        { method: RequestMethod.DELETE, path: '/report/delete' }
      )
    }
}