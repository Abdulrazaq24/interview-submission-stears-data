import * as mongoose from 'mongoose';

export const ReportSchema = new mongoose.Schema({
  title: String,
  period: String,
  year: Number,
  assignee: String,
  deadline: String,
  postId: String,
  submitted: Boolean,
  content: String,
  createdAt: Date,
});
