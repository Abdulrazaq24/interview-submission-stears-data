import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Report } from './interfaces/report.interface';
import { CreateReportDTO } from './dto/create-report.dto';

@Injectable()
export class ReportService {
  constructor(@InjectModel('Report') private readonly reportModel: Model<Report>) { }

  async getReports(): Promise<Report[]> {
    const reports = await this.reportModel.find().exec();
    return reports;
  }

  async getReport(reportID): Promise<Report> {
    const report = await this.reportModel
      .findById(reportID)
      .exec();
    return report;
  }

  async addReport(createReportDTO: CreateReportDTO): Promise<Report> {
    const newReport = await new this.reportModel(createReportDTO);
    return newReport.save();
  }

  async editReport(reportID, createReportDTO: CreateReportDTO): Promise<Report> {
    const editedReport = await this.reportModel
      .findByIdAndUpdate(reportID, createReportDTO, { new: true });
    return editedReport;
  }

  async deleteReport(reportID): Promise<any> {
    const deletedReport = await this.reportModel
      .findByIdAndRemove(reportID);
    return deletedReport;
  }
}
