import * as mongoose from 'mongoose';

export const CompanySchema = new mongoose.Schema({
  name: String,
  description: String,
  address: String,
  email: String,
  reportsCount: Number,
  author: String,
  createdAt: Date
});
