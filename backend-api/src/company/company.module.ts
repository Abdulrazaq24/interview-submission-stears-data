import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { CompanyService } from './company.service';
import { CompanyController } from './company.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CompanySchema } from './schemas/company.schema';
import { AuthenticationMiddleware } from '../common/authentication.middleware';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Post', schema: CompanySchema }]),
    ],
  providers: [CompanyService],
  controllers: [CompanyController]
})
export class CompanyModule implements NestModule {
    configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
      consumer.apply(AuthenticationMiddleware).forRoutes(
        { method: RequestMethod.POST, path: '/company/post' },
        { method: RequestMethod.PUT, path: '/company/edit' },
        { method: RequestMethod.DELETE, path: '/company/delete' }
      )
    }
}