
import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Put, Query, Delete, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CompanyService } from './company.service';
import { CreatePostDTO } from './dto/create-post.dto';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';
import { AuthGuard } from '@nestjs/passport';

@ApiBearerAuth()
@ApiTags('company')
@Controller('company')
export class CompanyController {
  constructor(private companyService: CompanyService) { }

  // Fetch all posts
  @Get('posts')
  @ApiResponse({ status: 200, description: 'The found records', type: CreatePostDTO })
  async getPosts(@Res() res) {
    const posts = await this.companyService.getPosts();
    return res.status(HttpStatus.OK).json(posts);
  }

  // Fetch all posts
  @Get('/search')
  @ApiResponse({ status: 200, description: 'The found records', type: CreatePostDTO })
  async searchPost(@Res() res, @Req() req) {
    const post = await this.companyService.searchPost(req, res);
    return res.status(HttpStatus.OK).json(post);
  }

  // Fetch a particular post using ID
  @Get('post/:postID')
  @ApiResponse({ status: 200, description: 'The found record', type: CreatePostDTO })
  async getPost(@Res() res, @Param('postID', new ValidateObjectId()) postID) {
    const post = await this.companyService.getPost(postID);
    if (!post) {
        throw new NotFoundException('Post does not exist!');
    }
    return res.status(HttpStatus.OK).json(post);
  }

  // Submit a post
  @Post('/post')
  @ApiOperation({ summary: 'Submit a company' })
  @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async addPost(@Res() res, @Body() createPostDTO: CreatePostDTO) {
    const newPost = await this.companyService.addPost(createPostDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Post has been submitted successfully!',
      post: newPost,
    });
  }

  @Put('/edit')
  @ApiResponse({ status: 200, description: 'Resource updated successfully' })
  async editPost(
    @Res() res,
    @Query('postID', new ValidateObjectId()) postID,
    @Body() createPostDTO: CreatePostDTO,
  ) {
    const editedPost = await this.companyService.editPost(postID, createPostDTO);
    if (!editedPost) {
        throw new NotFoundException('Post does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Post has been successfully updated',
      post: editedPost,
    });
  }

  // Delete a post using ID
  @Delete('/delete')
  @ApiResponse({ status: 200, description: 'Resource deleted successfully' })
  async deletePost(@Res() res, @Query('postID', new ValidateObjectId()) postID) {
    const deletedPost = await this.companyService.deletePost(postID);
    if (!deletedPost) {
        throw new NotFoundException('Post does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Post has been deleted!',
      post: deletedPost,
    });
  }
}
