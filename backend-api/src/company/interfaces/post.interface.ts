import { Document } from 'mongoose';

export interface Post extends Document {
  readonly name: string;
  readonly description: string;
  readonly address: string;
  readonly email: string;
  // readonly reports: [];
  readonly reportsCount: number;
  readonly author: string;
  readonly createdAt: Date | string;
}