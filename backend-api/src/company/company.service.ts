import { Res, Req } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Post } from './interfaces/post.interface';
import { CreatePostDTO } from './dto/create-post.dto';

@Injectable()
export class CompanyService {
  constructor(@InjectModel('Post') private readonly postModel: Model<Post>) { }

  async getPosts(): Promise<Post[]> {
    const posts = await this.postModel.find()
    .exec();
    return posts;
  }

  async searchPost(@Res() res, @Req() req): Promise<Post[]> {
    const { search_field, search_value } = req.query;
    const queryObj = {};
    if (search_field !== '' && search_value !== '') {
      queryObj[search_field] = search_value;
    }

    const post = await this.postModel.find(queryObj).exec();

    if (!post) {
      return res.status(404).json({
        status: 'failure',
        message: `Post with the given ${search_field}:${search_value} not found`
      });
    }

    return post;
  }

  async getPost(postID): Promise<Post> {
    const post = await this.postModel.findById(postID).exec();
    return post;
  }

  async addPost(createPostDTO: CreatePostDTO): Promise<Post> {
    const newPost = await new this.postModel(createPostDTO);
    return newPost.save();
  }

  async editPost(postID, createPostDTO: CreatePostDTO): Promise<Post> {
    const editedPost = await this.postModel
      .findByIdAndUpdate(postID, createPostDTO, { new: true });
    return editedPost;
  }

  async deletePost(postID): Promise<any> {
    const deletedPost = await this.postModel.findByIdAndRemove(postID);
    return deletedPost;
  }
}
