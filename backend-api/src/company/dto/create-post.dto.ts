import { IsInt, IsString, IsEmail, IsArray } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreatePostDTO {
    @IsString()
    @ApiProperty({ example: 'Stears Data', description: 'Name of the Company' })
    readonly name: string;

    @IsString()
    @ApiProperty({
        example: 'Building better data networks for Africa',
        description: 'Company description'
    })
    readonly description: string;

    @IsString()
    @ApiProperty({ example: 'Lagos, Nigeria', description: 'Address of the company' })
    readonly address: string;

    @IsEmail()
    @ApiProperty({ example: 'stears@stearsng.com', description: 'Email address of the company' })
    readonly email: string;

    // @IsArray()
    // @ApiProperty({ example: 'Reports[]', description: 'Collection of Company Reports' })
    // readonly reports: [];

    @IsInt()
    @ApiProperty({ example: '3', description: 'Number of report submitted to a Company' })
    readonly reportsCount: number;

    @IsString()
    @ApiProperty({ example: 'Abdulrazaq Imam', description: 'Company profiled by a User' })
    readonly author: string;

    @ApiProperty()
    @ApiProperty({ example: '05/05/2020', description: 'Date of company created' })
    readonly createdAt: Date;
}