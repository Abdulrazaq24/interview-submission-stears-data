import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { ReportModule } from '../../src/report/report.module';
import { ReportService } from '../../src/report/report.service';
import { CreateReportDTO } from 'src/report/dto/create-report.dto';

describe('Report', () => {

  const reportService = {
    getPosts: () => ['reports'] ,
    getPost: (reportID) => ['reports'],
    addPost: (createReportDTO: CreateReportDTO) => ['reports'],
    editPost: (reportID, createPostDTO: CreateReportDTO) => ['reports'],
    deletePost: (reportID) => ['reports']
  };
  let server;
  let app: INestApplication;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
        imports: [ReportModule],
    })
    .overrideProvider(ReportService)
    .useValue(reportService)
    .compile();

    app = module.createNestApplication();
    server = app.getHttpServer();
    await app.init();
  });

  it(`/GET reports`, () => {
    return request(server).get('/report/reports').expect(200).expect({
      data: this.reportService.getPosts(),
    });
  });

  it(`/GET report`, (reportID) => {
    return request(server).get('/report/report').expect(200).expect({
      data: this.reportService.getReport(reportID),
    });
  });

  it(`/POST report`, (createReportDTO) => {
    return request(server).post('/report/report').expect(200).expect({
      data: this.reportService.addReport(createReportDTO),
    });
  });

  it(`/PUT /edit`, (reportID) => {
    return request(server).put('/report/report/:reportId').expect(200).expect({
      data: this.reportService.editReport(reportID),
    });
  });

  it(`/DELETE /delete`, (reportID) => {
    return request(server).delete('/report/delete/:reportId').expect(200).expect({
      data: this.reportService.deleteReport(reportID),
    });
  });

  afterAll(async () => {
    await app.close();
  });
});