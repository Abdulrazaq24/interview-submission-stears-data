import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { CompanyModule } from '../../src/company/company.module';
import { CompanyService } from '../../src/company/company.service';
import { CreatePostDTO } from 'src/company/dto/create-post.dto';

describe('Company', () => {

  const companyService = {
    getPosts: () => ['posts'] ,
    getPost: (postID) => ['posts'],
    addPost: (createPostDTO: CreatePostDTO) => ['posts'],
    editPost: (postID, createPostDTO: CreatePostDTO) => ['posts'],
    deletePost: (postID) => ['posts']
  };
  let server;
  let app: INestApplication;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
        imports: [CompanyModule],
    })
    .overrideProvider(CompanyService)
    .useValue(companyService)
    .compile();

    app = module.createNestApplication();
    server = app.getHttpServer();
    await app.init();
  });

  it(`/GET posts`, () => {
    return request(server).get('/company/posts').expect(200).expect({
      data: this.companyService.getPosts(),
    });
  });

  it(`/GET post`, (postID) => {
    return request(server).get('/company/post').expect(200).expect({
      data: this.companyService.getPost(postID),
    });
  });

  it(`/POST post`, (createPostDTO) => {
    return request(server).post('/company/post').expect(200).expect({
      data: this.companyService.addPost(createPostDTO),
    });
  });

  it(`/PUT /edit`, (postID) => {
    return request(server).put('/company/post/:postId').expect(200).expect({
      data: this.companyService.editPost(postID),
    });
  });

  it(`/DELETE /delete`, (postID) => {
    return request(server).delete('/company/delete/:postId').expect(200).expect({
      data: this.companyService.deletePost(postID),
    });
  });

  afterAll(async () => {
    await app.close();
  });
});