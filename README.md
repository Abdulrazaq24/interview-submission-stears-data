# Stears Coding Homework: Using Nest.js, TypeScript, React and MongoDB

Interview submission repo for a simple company aggregtion application built with MongoDB, ReactJS, Typescript, NodeJS & NestJS.

## Getting Started
This submission contains two separate sections. Namely Task1: The Backend API ( built with Nest.js, MongoDB, Typescript and NodeJS ) and Task 2: The Front-End UI/UX implementation
( Built with ReactJS and Typescript ).

### Install Global Dependencies
Install TypeScript globally on your machine if you don't have it installed already:

```bash
npm install -g typescript
```

### Clone the repository
To help set up working application, clone this repository which contains directory for both sections of the project ( i.e `backend-api` and `frontend-data`)

```bash
git clone https://github.com/abdulrazaq24/interview-submission-stears-data.git
```

## Change directory into the newly cloned project
```bash
cd interview-submission-stears-data
```

## Backend API [Task 1]
### Change directory into the Backend API
```bash
cd backend-api
```

### Install Backend API dependencies

```bash
npm install
```

### Create .env file
Once the installation process is complete, create a `.env` file:

```bash
touch .env
```

Open the newly created file i.e .env file and add the following code:

```
AUTH0_DOMAIN=YOUR_AUTH0_DOMAIN
AUTH0_AUDIENCE=YOUR_AUTH0_AUDIENCE
```

Ensure that you replace the `YOUR_AUTH0_DOMAIN` and `YOUR_AUTH0_AUDIENCE` placeholder with the appropriate credentials as obtained from your [Auth0 dashboard](https://auth0.com/).

#### How to Setup Auth0 to manage the identity of users/authentication
After creating an account from [Auth0](https://auth0.com/) respectively, log in to it and head over to the API section of your Auth0 management dashboard and select APIs from the side menu. This will show your list of APIs if you have created any from your account. Click on the CREATE API button and set up a new one. Next, provide a friendly name as you deem fit for your API. Set the identifier as http://localhost:5000/api (5000 is our backend port), leave the signing algorithm as RS256 and click on the CREATE button to proceed.

Next, click on "Applications" from the side menu to view the list of applications for your account and select the test application that was generated. You will then click on the "Settings" tab.

Now locate the Allowed Callback URLs fields and add http://localhost:3000 (3000 is our frontend port) as the value, which will be used as our frontend address. Save the changes to proceed. Don’t forget to copy the Domain, Client ID, and Client Secret as you will need it soon to configure your API.

To easily access and reference credentials such as the Domain for this application on Auth0, create a .env (if you haven't already) within the root of your backend application and paste the following content:

```
AUTH0_DOMAIN=YOUR_AUTH0_DOMAIN
AUTH0_AUDIENCE=http://localhost:5000/api
```

Replace the YOUR_AUTH0_DOMAIN with the credential obtained from your Auth0 dashboard  named as 'Domain' field.


### MongoDB
Ensure that you have [mongoDB](https://mongodb.com/) installed on your machine before running the application. I have this fully setup on my mac already.

Start mongoDB:

```bash
sudo mongod
```

### Run the application
Open another terminal and still within the `backend-api` project directory run the application with:

```bash
npm run start:dev
```

![Screenshot](../backend-api/backend-engine.png?raw=true "Screenshot")

This will start the backend application on port `5000`. This was modified to avoid confliction with the frontend application which by default will run on port `3000`

### Test the application
To test the Backend API, run the unit test with:

```bash
npm run test:e2e
```

### Test the application
There is a docker-compose.yml file for starting Docker.

```bash
docker-compose up
```

After running the sample, you can stop the Docker container with

```bash
docker-compose down
```

### Swagger Open API documentation
Once the application is running you can visit http://localhost:5000/api to see the Swagger interface.

See [here](https://docs.nestjs.com/recipes/swagger#bootstrap) for more information.


## Frontend Data [Task 2]
Open another terminal from the `interview-submission-stears-data` and navigate to the `frontend-data` folder to setup the frontend

### Frontend dependencies
```bash
cd frontend-data
npm install
```

### Create .env file and include Auth0 App credentials

In the same directory, create a `.env` file as shown:

```
touch .env
```

Open the file and paste the following code in it:

```
REACT_APP_AUTH0_CLIENT_ID=YOUR_AUTH0_CLIENT_ID
REACT_APP_AUTH0_DOMAIN=YOUR_AUTH0_DOMAIN
REACT_APP_AUTH0_AUDIENCE=YOUR_AUTH0_AUDIENCE
REACT_APP_AUTH0_REDIRECT_URI=http://localhost:3000
REACT_APP_BASEURL=http://localhost:3000
```

Replace `YOUR_AUTH0_CLIENT_ID`, `YOUR_AUTH0_DOMAIN` and `YOUR_AUTH0_AUDIENCE` placeholder with your Auth0 credentials created earlier.

### Run the Frontend Data app

```bash
npm start
```

### Test the application
Finally open your browser and view the application on http://localhost:3000

![Screenshot](../frontend-data/demo/frontend-ui.png?raw=true "Screenshot")
![Screenshot](../frontend-data/demo/frontend-ui-2.png?raw=true "Screenshot")
![Screenshot](../frontend-data/demo/frontend-ui-3.png?raw=true "Screenshot")
![Screenshot](../frontend-data/demo/frontend-ui-4.png?raw=true "Screenshot")


## Search [Task 3]
Added a search bar to the home page that allows anyone to perform full text search.

Searches goes to the backend with an endpoint http://localhost:5000/search?

## Prerequisites
 [Node.js](https://nodejs.org/en/), [Yarn package manager](https://yarnpkg.com/lang/en/docs/install/#mac-stable), [MongoDB](https://docs.mongodb.com/v3.2/installation/) and [TypeScript](https://www.typescriptlang.org/)


## Built With
[Nest.js](https://nestjs.com/)
[React.js](https://reactjs.org/)
[Auth0](https://autth0.com/) 
[TypeScript](https://typescriptlang.org/)
[MongoDB](https://mongodb.com/) 

