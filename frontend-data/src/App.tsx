import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Post from './components/post/Post';
import Edit from './components/post/Edit';
import Create from './components/post/Create';
import Add from './components/report/Add';
import Report from './components/report/Report';
import Modify from './components/report/Modify';


function App(): JSX.Element {

  return (
    <div className="App">
      <Navbar />
      <div>
        <Switch>
          <Route path={"/"} exact={true} component={Home} />
          <Route path={"/post/:postId"} component={Post}/>
          <Route path={"/edit/:postId"} component={Edit}/>
          <Route path={"/create"} component={Create} />
          <Route path={"/add-report/:postId"} component={Add} />
          <Route path={"/report/:reportId"} component={Report} />
          <Route path={"/report-edit/:reportId"} component={Modify} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
