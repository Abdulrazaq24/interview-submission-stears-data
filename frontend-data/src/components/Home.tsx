import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useAuth0 } from '../contexts/auth0-context';


function Home():JSX.Element {
  
  let history = useHistory()
  const { isAuthenticated, getIdTokenClaims, user } = useAuth0();

  const [posts, setPosts] = useState();

  const deletePost = async(id: string) => {
    const accessToken = await getIdTokenClaims();
    await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/delete?postID=${id}`, {
      method: "delete",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json",
        "authorization": `Bearer ${accessToken.__raw}`
      })
    });
    _removePostFromView(id);
    history.push('/');
  }

  const _removePostFromView = (id: string) => {
    const index = posts.findIndex((post: { _id: string; }) => post._id === id);
    posts.splice(index, 1);
  }

  useEffect(() => {
    const fetchPosts = async (): Promise<any> => {
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/posts`);
      const json = await response.json();
      setPosts(json)
    }
    fetchPosts();
  }, [])

    return (
      <div>
        <div className="bg-gradient text-white o-hidden" data-overlay>
          <section className="pb-5 pt-5"> </section>
        </div>

        <section className="bg-light pt-6">
          <div className="container">
            <div className="row section-title justify-content-center text-center">
              <div className="col-md-9 col-lg-8 col-xl-7">
                <h3 className="display-5">Profiled Companies</h3>
                <div className="lead">
                  To browse through company for a detailed profile and reports, please click on 
                  view.
                </div>
              </div>
            </div>

            <div className="row justify-content-center">
              <div className="col-xl-10 col-lg-12">
                <div className="row justify-content-center">
                  
                  {posts && posts.map((post: { name: React.ReactNode; _id: any; description: any; address: any; email: any; reportsCount: any; reports: []; author: any; }) => (
                    <div className="col-md-4 mb-3 mb-md-4" key={post._id}>
                      <div className="card h-100">
                        <div className="d-block bg-gradient rounded-top position-relative">
                          <img className="card-img-top hover-fade-out" src="img/Home_cover.jpg" alt="accompanying Circle testimonial" />
                          <div className="badge badge-light">
                            No. of Reports: {post.reportsCount | 0}
                          </div>
                        </div>

                        <div className="card-body">
                          <h3>{post.name}</h3>
                          <p>
                            {post.description}
                          </p>
                          <p className="">{post.address} <br/>
                          {post.email}</p>

                          <p className="">Posted By: {post.author}</p>

                          <Link to={`/post/${post._id}`} className="btn btn-success btn-sm"> View </Link> {" "}
                          {
                            isAuthenticated && (user.name === post.author) &&
                            <Link to={`/edit/${post._id}`} className="btn btn-info btn-sm"> Edit </Link>
                          } 
                          {" "}
                          {
                            isAuthenticated && (user.name === post.author) &&
                            <button className="btn btn-danger btn-sm" onClick={() => deletePost(post._id)}>Delete</button>
                          }
                        </div>
                      </div>
                    </div>
                  ))}

                </div>

                <div className="row justify-content-center mt-3 mt-md-4">
                  <div className="col-auto">
                    <button className="btn btn-primary text-white" onClick={() => ("/")}>Load more...</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
}

export default Home;
