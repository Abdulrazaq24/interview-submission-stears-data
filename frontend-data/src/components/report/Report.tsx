import React, { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { useParams } from 'react-router';
import { useAuth0 } from '../../contexts/auth0-context';


function Report(): JSX.Element {
  const { isAuthenticated, user } = useAuth0();

  let { reportId } = useParams();
  const [report, setReport] = useState<any>({});

  useEffect(() => {
    const fetchReport = async (): Promise<any> => {
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/report/report/${reportId}`);
      const json = await response.json();
      setReport(json)
    }
    fetchReport();
  }, [reportId])

  return (
    <div>
      <div className="bg-gradient text-white o-hidden" data-overlay>
        <section className="pb-5 pt-5"> </section>
      </div>

      <section className="bg-light">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-9 col-lg-10">
              <div className="card card-body shadow-lg">
                <div className="d-flex justify-content-between align-items-start pb-4 pb-md-5 mb-4 mb-md-5 border-bottom">
                  <div> </div>

                  {
                    isAuthenticated && (user.name === report.assignee) &&
                    <Link to={`/report-edit/${reportId}`} className="btn btn-success btn-sm">Edit Report</Link>
                  } 
                </div>
                <div className="mb-4">
                <h2>{report.title} <span className="badge badge-success">SUBMITTED: {report.submitted}</span></h2>
                </div>
                <div className="row justify-content-between mb-4 mb-md-5">
                  <div className="col-sm">
                    <h6>Report Brief</h6>
                    <div>
                      <div>{report.content}</div>
                    </div>
                  </div>

                  <div className="col-sm col-lg-4">
                    <dl className="row text-sm-right">
                      <dt className="col-6"><strong>Period:</strong></dt>
                      <dd className="col-6">{report.period}</dd>
                      <dt className="col-6"><strong>Year:</strong></dt>
                      <dd className="col-6">{report.year}</dd>
                      <dt className="col-6"><strong>Assignee:</strong></dt>
                      <dd className="col-6">{report.assignee}</dd>
                      <dt className="col-6"><strong>Date Created:</strong></dt>
                      <dd className="col-6">{report.createdAt}</dd>
                      <dt className="col-6"><strong>Date Due:</strong></dt>
                      <dd className="col-6">{report.deadline}</dd>
                      <dt className="col-6"><strong>Status</strong></dt>
                      <dd className="col-6">{report.submitted}</dd>
                    </dl>
                  </div>
                </div>

                <div>
                  <div className="text-small">
                    <div>
                      <Link to="/">Stears Data</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );

}
export default withRouter(Report)
