import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useAuth0 } from '../../contexts/auth0-context';


function Modify(): JSX.Element {

  const { getIdTokenClaims } = useAuth0();

  let history = useHistory();
  let { reportId } = useParams();

  interface IValues {
    [key: string]: any;
  }

  const [report, setReport] = useState()
  const [values, setValues] = useState<IValues>([]);
  const [submitSuccess, setSubmitSuccess] = useState<boolean>(false)
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchReport = async (): Promise<void> => {
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/report/report/${reportId}`);
      const json = await response.json();
      setReport(json)    
    }
    fetchReport();    
  }, [reportId]);

  const handleFormSubmission = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    setLoading(true);

    const submitSuccess: boolean = await submitForm();
    setSubmitSuccess(submitSuccess);
    setLoading(false);

    setTimeout(() => {
      history.push(`/report/${reportId}`);
    }, 1500);
  }

  const submitForm = async (): Promise<boolean> => {
    try {
      const accessToken = await getIdTokenClaims();
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/report/edit?reportID=${reportId}`, {
        method: "put",
        headers: new Headers({
          "Content-Type": "application/json",
          Accept: "application/json",
          "authorization": `Bearer ${accessToken.__raw}`
        }),
        body: JSON.stringify(values)
      });
      return response.ok;      
    } catch(ex) {
      return false;
    }
  }

  const setFormValues = (formValues: IValues) => {
    setValues({...values, ...formValues})
  }

  const handleInputChanges = (e: React.FormEvent<HTMLInputElement>) => {
    setFormValues({ [e.currentTarget.id]: e.currentTarget.value })
  }
  
  const handleTextAreaChanges = (e: React.FormEvent<HTMLTextAreaElement>) => {
    setFormValues({ [e.currentTarget.id]: e.currentTarget.value })
  }

  const handleSelectChanges = (e: React.FormEvent<HTMLSelectElement>) => {
    setFormValues({ [e.currentTarget.id]: e.currentTarget.value })
  }

  return (
    <div>
      <div className="bg-gradient text-white o-hidden" data-overlay>
        <section className="pb-5 pt-5"> </section>
      </div>

      <section className="pt-6">
        {report &&
          <div className="container">
            <div className="row section-title justify-content-center text-center">
              <div className="col-md-7 col-lg-6 col-xl-4">
                <h3 className="display-5">Edit Report</h3>
              </div>
            </div>

            <div className="row justify-content-center">
              <div className="col-xl-6 col-lg-4 col-md-6">

                <form className="d-flex flex-column" onSubmit={handleFormSubmission} noValidate={true}>

                  {submitSuccess && (
                    <div className="alert alert-success w-100 my-md-3" role="alert">
                      <p>Report has been edited successfully!</p>
                    </div>
                  )}

                  <div className="col mb-4 mb-md-5">
                    <h6 className="lead">Provide report details</h6>
                    <div className="d-flex flex-wrap">
                      <input 
                      type="text" 
                      id="title" 
                      defaultValue={report.title} 
                      onChange={(e) => handleInputChanges(e)} 
                      name="title" 
                      className="form-control h-100" />

                      <select 
                      id="period" 
                      onChange={(e) => handleSelectChanges(e)} 
                      name="period" 
                      className="custom-select mt-3">
                        <option defaultValue={report.period}>{report.period}</option>
                        <option value="Q1">Q1</option>
                        <option value="Q2">Q2</option>
                        <option value="Q3">Q3</option>
                        <option value="Q4">Q4</option>
                      </select>

                      <input 
                      type="number" 
                      id="year" 
                      defaultValue={report.year} 
                      onChange={(e) => handleInputChanges(e)} 
                      name="year" 
                      className="form-control h-100 my-3"
                      placeholder="Report period e.g 2020" />

                      <input 
                      type="text" 
                      id="deadline" 
                      defaultValue={report.deadline} 
                      onChange={(e) => handleInputChanges(e)} 
                      name="deadline" 
                      className="form-control h-100" />

                      <textarea 
                      id="content" 
                      defaultValue={report.content} 
                      onChange={(e) => handleTextAreaChanges(e)} 
                      name="content" 
                      className="form-control h-100 my-3" />
                    </div>
                  </div>
                  
                  <button 
                  className="btn btn-primary flex-shrink-0" 
                  type="submit">
                    Update Report
                    {loading &&
                      <span className="fa fa-circle-o-notch fa-spin" />
                    }
                  </button>
                </form>
                
              </div>
            </div>
          </div>
        }
      </section>
    </div>
  )
}

export default Modify;