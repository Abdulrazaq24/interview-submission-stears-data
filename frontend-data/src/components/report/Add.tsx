import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { useParams } from 'react-router'
import { useAuth0 } from '../../contexts/auth0-context';


function Add(): JSX.Element {

  let history = useHistory();
  const { user, getIdTokenClaims } = useAuth0();

  let { postId } = useParams()
  const [post, setPost] = useState<any>({});
  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/post/${postId}`);
      const json = await response.json();
      setPost(json);
    }
    fetchData();
  }, [postId]);


  interface IValues {
    [key: string]: any;
  }

  const [author, setAuthor] = useState<string>('');
  const [values, setValues] = useState<IValues>([]);
  const [submitSuccess, setSubmitSuccess] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    if (user) {
      setAuthor(user.name)
    }
  }, [user])

  const handleFormSubmission = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    setLoading(true);

    const formData = {
      title: values.title,
      period: values.period,
      year: values.year,
      assignee: author,
      deadline: values.deadline,
      postId: postId,
      content: values.content,
      submitted: "true",
      createdAt: new Date().toString()
    }

    const submitSuccess: boolean = await submitform(formData);
    setSubmitSuccess(submitSuccess);
    setValues({...values, formData});
    setLoading(false);
    setTimeout(() => {
      history.push(`/post/${post._id}`);
    }, 1500);
  }

  const submitform = async (formData: {}) => {
    try {
      const accessToken = await getIdTokenClaims();
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/report/report`, {
        method: "post",
        headers: new Headers({
          "Content-Type": "application/json",
          "Accept": "application/json",
          "authorization": `Bearer ${accessToken.__raw}`
        }),
        body: JSON.stringify(formData)
      });
      return response.ok;
    } catch (ex) {
      return false;
    }
  }

  const setFormValues = (formValues: IValues) => {
    setValues({...values, ...formValues})
  }

  const handleInputChanges = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault();
    setFormValues({ [e.currentTarget.name]: e.currentTarget.value })
  }

  const handleTextAreaChanges = (e: React.FormEvent<HTMLTextAreaElement>) => {
    e.preventDefault();
    setFormValues({ [e.currentTarget.name]: e.currentTarget.value })
  }

  const handleSelectChanges = (e: React.FormEvent<HTMLSelectElement>) => {
    e.preventDefault();
    setFormValues({ [e.currentTarget.name]: e.currentTarget.value })
  }

  return (
    <div>
      <div className="bg-gradient text-white o-hidden" data-overlay>
        <section className="pb-5 pt-5"> </section>
      </div>

      <section className="pt-6">
        <div className="container">
          <div className="row section-title justify-content-center text-center">
              {post &&
                <div className="col-md-7 col-lg-6 col-xl-4">
                  <h3 className="display-5">Add Report to [{post.name}]</h3>
                </div>
              }
          </div>

          <div className="row justify-content-center">
            <div className="col-xl-6 col-lg-4 col-md-6">

              <form className="d-flex flex-column" onSubmit={handleFormSubmission} noValidate={true}>

                {!submitSuccess && (
                  <div className="alert alert-info w-100 my-md-3" role="alert">
                    <p>Fill the form below to create a new report</p>
                  </div>
                )}

                {submitSuccess && (
                  <div className="alert alert-success w-100 my-md-3" role="alert">
                    Report was successfully submitted!
                  </div>
                )}

                <div className="col mb-4 mb-md-5">
                  <h6 className="lead">Provide report details</h6>
                  <div className="d-flex flex-wrap">

                    <input 
                    type="text" 
                    id="title" 
                    onChange={(e) => handleInputChanges(e)} 
                    name="title" 
                    className="form-control h-100 my-3"
                    placeholder="Enter report title" />

                    <select 
                    id="period" 
                    onChange={(e) => handleSelectChanges(e)} 
                    name="period" 
                    className="custom-select">
                      <option defaultValue="">Select an option</option>
                      <option value="Q1">Q1</option>
                      <option value="Q2">Q2</option>
                      <option value="Q3">Q3</option>
                      <option value="Q4">Q4</option>
                    </select>

                    <input 
                    type="number" 
                    id="year" 
                    onChange={(e) => handleInputChanges(e)} 
                    name="year" 
                    className="form-control h-100 my-3"
                    placeholder="Report period e.g 2020" />

                    <label className="mb-0">Assignee</label>
                    <input 
                    type="text" 
                    id="assignee" 
                    defaultValue={author} 
                    onChange={(e) => handleInputChanges(e)} 
                    name="assignee" 
                    className="form-control h-100" 
                    disabled />

                    <input 
                    type="text" 
                    id="deadline" 
                    onChange={(e) => handleInputChanges(e)} 
                    name="deadline" 
                    className="form-control h-100 my-3"  
                    placeholder="report deadline e.g 12th Fe." />

                    <textarea  
                    id="content" 
                    onChange={(e) => handleTextAreaChanges(e)} 
                    name="content" 
                    className="form-control h-100 my-3"  
                    placeholder="Begin writing report" />

                  </div>
                </div>
                
                <button 
                className="btn btn-primary flex-shrink-0" 
                type="submit">
                  Submit Report
                  {loading &&
                    <span className="fa fa-circle-o-notch fa-spin" />
                  }
                </button>
              </form>
              
            </div>
          </div>
        </div>
      </section>
    </div>
  );

}
export default withRouter(Add)
