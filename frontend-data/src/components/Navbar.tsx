import React, {useEffect, useState} from 'react';
import { Link, withRouter, useHistory } from 'react-router-dom';
import { useParams } from 'react-router';
import { useAuth0 } from '../contexts/auth0-context';

function Navbar() {
    let history = useHistory()
    const { isLoading, user, loginWithRedirect, logout, isAuthenticated } = useAuth0();

    let { postId } = useParams();
    const [post, setPost] = useState<any>({});

    useEffect(() => {
        const fetchData = async (): Promise<void> => {
          const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/post/${postId}`);
          const json = await response.json();
          setPost(json);
        }
        fetchData();
      }, [postId]);

    const searchPost = async(id: string, search: string) => {
        await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/search?search_field=${id}?search_value=${search}`, {
          method: "get",
          headers: new Headers({
            "Content-Type": "application/json",
            Accept: "application/json"
          })
        });
        history.push("/");
      }

    return (
        <div className="navbar-container">
            <nav className="navbar navbar-expand-lg navbar-dark" data-overlay>
                <div className="container mt-2">
                    <Link className="navbar-brand navbar-brand-dynamic-color fade-page" to={"/"}> Stears Data </Link>

                    <div className="d-flex align-items-center order-lg-3">
                        {!isLoading && !user && (
                            // eslint-disable-next-line
                            <a 
                            href="" 
                            onClick={loginWithRedirect} 
                            className="nav-link fade-page px-0 py-2 text-white ml-lg-4" 
                            style={{"fontSize": "1.2rem"}}>Sign In</a>
                        )}

                        {!isLoading && user && (
                            <>
                                <label className="nav-link fade-page px-0 py-2 text-white ml-lg-4 mb-0">{user.name}</label>
                                {/* eslint-disable-next-line */}
                                <a 
                                href="" 
                                onClick={() => logout({ returnTo: window.location.origin })} 
                                className="nav-link fade-page px-0 py-2 text-white ml-lg-4"
                                style={{"fontSize": "1.2rem"}}>Signout</a>
                            </>
                        )}
                    </div>

                    {isAuthenticated && (

                        <div className="d-flex align-items-center order-lg-3">
                            <Link className="btn btn-primary ml-lg-4 mr-3 mr-md-4 mr-lg-0 d-none d-sm-block order-lg-3" to={"/create"}> Create </Link>
                            <button aria-expanded="false" aria-label="Toggle navigation" className="navbar-toggler" data-target=".navbar-collapse" data-toggle="collapse" type="button">
                            <img alt="Navbar Toggler Open Icon" className="navbar-toggler-open icon icon-sm"  src="img/icons/interface/icon-menu.svg" />
                            <img alt="Navbar Toggler Close Icon" className="navbar-toggler-close icon icon-sm"  src="img/icons/interface/icon-x.svg" />
                            </button>
                        </div>
                    )}

                    <div className="d-flex align-items-center order-lg-3 ml-3">
                        <div className="">
                            <div className="form-group mb-0">
                            {post &&
                                <div className="input-group mb-0">
                                
                                    <div className="input-group-prepend">
                                        <span className="input-group-text bg-primary border-primary">
                                            <img alt="Search" className="icon icon-sm" src="/img/icons/interface/icon-search.svg" />
                                        </span>
                                    
                                    </div>
                                
                                    <input 
                                    className="form-control" 
                                    placeholder="Search" 
                                    type="search"
                                    onClick={() => searchPost(post._id, post.search)} />
                                </div>
                            }
                            </div>
                        </div>
                    </div>

                    <div className="collapse navbar-collapse order-3 order-lg-2 justify-content-lg-end" id="navigation-menu">
                        <ul className="navbar-nav my-3 my-lg-0">
                            <li className="nav-item">
                                <Link className="nav-link nav-item" role="button" to={"/"}> Home </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        // <header>
        //     <div className="container-fluid position-relative no-side-padding">
        //         <span className="logo">
        //         {user && user.picture && <img src={user.picture} alt="My Avatar" />}
        //         {!user && <img src={'https://res.cloudinary.com/yemiwebby-com-ng/image/upload/v1513770253/WEB_FREAK_50PX-01_yaqxg7.png'} alt="My Avatar" />}
        //         </span>

        //         <div className="menu-nav-icon" data-nav-menu="#main-menu">
        //             <i className="ion-navicon" />
        //         </div>

        //         <ul className="main-menu visible-on-click" id="main-menu">
        //             <li><Link className={"nav-link"} to={"/"}> Nest React TypeScript Blog </Link></li>
        //             <li>
        //             <Link className={"nav-link"} to={"/"}>
        //                 {!isLoading && !user && (
        //                     <>
        //                         <button className="btn btn-dark" onClick={loginWithRedirect}>
        //                             Sign In
        //                         </button>
        //                     </>
        //                 )}

        //                 {!isLoading && user && (
        //                     <>
        //                         <div>
        //                             <label className="mr-2">{user.name}</label>
        //                             <button className="btn btn-dark" onClick={() => logout({ returnTo: window.location.origin })}>
        //                                 Sign Out 
        //                             </button>
        //                         </div>
        //                     </>
        //                 )}
        //             </Link>
        //             </li>
        //             <li><Link className={"nav-link"} to={"/"}> Home </Link></li>
        //             {isAuthenticated && (
        //             <li><Link className={"nav-link"} to={"/create"}> Create </Link></li>
        //             )}
        //         </ul>
        //     </div>
        // </header>
    );
}

export default withRouter(Navbar);
