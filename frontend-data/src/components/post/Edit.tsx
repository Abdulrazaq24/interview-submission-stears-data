import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useAuth0 } from '../../contexts/auth0-context';


function Edit(): JSX.Element {

  const { getIdTokenClaims } = useAuth0();

  let history = useHistory();
  let { postId } = useParams();

  interface IValues {
    [key: string]: any;
  }

  const [post, setPost] = useState()
  const [values, setValues] = useState<IValues>([]);
  const [submitSuccess, setSubmitSuccess] = useState<boolean>(false)
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/post/${postId}`);
      const json = await response.json();
      setPost(json)    
    }
    fetchData();    
  }, [postId]);

  const handleFormSubmission = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    setLoading(true);

    const submitSuccess: boolean = await submitForm();
    setSubmitSuccess(submitSuccess);
    setLoading(false);

    setTimeout(() => {
      history.push('/');
    }, 1500);
  }

  const submitForm = async (): Promise<boolean> => {
    try {
      const accessToken = await getIdTokenClaims();
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/edit?postID=${postId}`, {
        method: "put",
        headers: new Headers({
          "Content-Type": "application/json",
          Accept: "application/json",
          "authorization": `Bearer ${accessToken.__raw}`
        }),
        body: JSON.stringify(values)
      });
      return response.ok;      
    } catch(ex) {
      return false;
    }
  }

  const setFormValues = (formValues: IValues) => {
    setValues({...values, ...formValues})
  }

  const handleInputChanges = (e: React.FormEvent<HTMLInputElement>) => {
    setFormValues({ [e.currentTarget.id]: e.currentTarget.value })
  }

  const handleTextAreaChanges = (e: React.FormEvent<HTMLTextAreaElement>) => {
    setFormValues({ [e.currentTarget.id]: e.currentTarget.value })
  }

  return (
    <div>
      <div className="bg-gradient text-white o-hidden" data-overlay>
        <section className="pb-5 pt-5"> </section>
      </div>

      <section className="pt-6">
        {post &&
          <div className="container">
            <div className="row section-title justify-content-center text-center">
              <div className="col-md-7 col-lg-6 col-xl-4">
                <h3 className="display-5">Edit Company</h3>
              </div>
            </div>

            <div className="row justify-content-center">
              <div className="col-xl-6 col-lg-4 col-md-6">

                <form className="d-flex flex-column" onSubmit={handleFormSubmission} noValidate={true}>

                  {submitSuccess && (
                    <div className="alert alert-success w-100 my-md-3" role="alert">
                      <p>The post has been edited successfully!</p>
                    </div>
                  )}

                  <div className="col mb-4 mb-md-5">
                    <h6 className="lead">Provide company details</h6>
                    <div className="d-flex flex-wrap">
                      <input 
                      type="text" 
                      id="name" 
                      defaultValue={post.name} 
                      onChange={(e) => handleInputChanges(e)} 
                      name="name" 
                      className="form-control h-100" />

                      <textarea 
                      id="description" 
                      defaultValue={post.description} 
                      onChange={(e) => handleTextAreaChanges(e)} 
                      name="description" 
                      className="form-control h-100 my-3" />

                      <input 
                      type="text" 
                      id="address" 
                      defaultValue={post.address} 
                      onChange={(e) => handleInputChanges(e)} 
                      name="address" 
                      className="form-control h-100 my-3" />

                      <input 
                      type="email" 
                      id="email" 
                      defaultValue={post.email} 
                      onChange={(e) => handleInputChanges(e)} 
                      name="email" 
                      className="form-control h-100" />
                    </div>
                  </div>
                  
                  <button 
                  className="btn btn-primary flex-shrink-0" 
                  type="submit">
                    Update Post
                    {loading &&
                      <span className="fa fa-circle-o-notch fa-spin" />
                    }
                  </button>
                </form>
                
              </div>
            </div>
          </div>
        }
      </section>
    </div>
  )
}

export default Edit;