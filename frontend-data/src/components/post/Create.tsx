import React, { useState, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { useAuth0 } from '../../contexts/auth0-context';


function Create(): JSX.Element {

  let history = useHistory();
  const { user, getIdTokenClaims } = useAuth0();

  interface IValues {
    [key: string]: any;
  }

  const [author, setAuthor] = useState<string>('');
  const [values, setValues] = useState<IValues>([]);
  const [submitSuccess, setSubmitSuccess] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    if (user) {
      setAuthor(user.name)
    }
  }, [user])

  const handleFormSubmission = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    setLoading(true);

    const formData = {
      name: values.name,
      description: values.description,
      address: values.address,
      email: values.email,
      createdAt: new Date().toString(),
      author: author
    }

    const submitSuccess: boolean = await submitform(formData);
    setSubmitSuccess(submitSuccess);
    setValues({...values, formData});
    setLoading(false);
    setTimeout(() => {
      history.push('/');
    }, 1500);
  }

  const submitform = async (formData: {}) => {
    try {
      const accessToken = await getIdTokenClaims();
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/post`, {
        method: "post",
        headers: new Headers({
          "Content-Type": "application/json",
          "Accept": "application/json",
          "authorization": `Bearer ${accessToken.__raw}`
        }),
        body: JSON.stringify(formData)
      });
      return response.ok;
    } catch (ex) {
      return false;
    }
  }

  const setFormValues = (formValues: IValues) => {
    setValues({...values, ...formValues})
  }

  const handleInputChanges = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault();
    setFormValues({ [e.currentTarget.name]: e.currentTarget.value })
  }

  const handleTextAreaChanges = (e: React.FormEvent<HTMLTextAreaElement>) => {
    e.preventDefault();
    setFormValues({ [e.currentTarget.name]: e.currentTarget.value })
  }

  return (
    <div>
      <div className="bg-gradient text-white o-hidden" data-overlay>
        <section className="pb-5 pt-5"> </section>
      </div>

      <section className="pt-6">
        <div className="container">
          <div className="row section-title justify-content-center text-center">
            <div className="col-md-7 col-lg-6 col-xl-4">
              <h3 className="display-5">Post/Create a Company</h3>
            </div>
          </div>

          <div className="row justify-content-center">
            <div className="col-xl-6 col-lg-4 col-md-6">

              <form className="d-flex flex-column" onSubmit={handleFormSubmission} noValidate={true}>

                {!submitSuccess && (
                  <div className="alert alert-info w-100 my-md-3" role="alert">
                    <p>Fill the form below to create a new post</p>
                  </div>
                )}

                {submitSuccess && (
                  <div className="alert alert-success w-100 my-md-3" role="alert">
                    The form was successfully submitted!
                  </div>
                )}

                <div className="col mb-4 mb-md-5">
                  <h6 className="lead">Provide company details</h6>
                  <div className="d-flex flex-wrap">
                    <input 
                    type="text" 
                    id="name" 
                    onChange={(e) => handleInputChanges(e)} 
                    name="name" 
                    className="form-control h-100"
                    placeholder="Enter company name" />

                    <textarea 
                    id="description" 
                    onChange={(e) => handleTextAreaChanges(e)} 
                    name="description" 
                    className="form-control h-100 my-3"
                    placeholder="Company description" />

                    <input 
                    type="text" 
                    id="address" 
                    onChange={(e) => handleInputChanges(e)} 
                    name="address" 
                    className="form-control h-100 my-3"
                    placeholder="Company address" />

                    <input 
                    type="email" 
                    id="email" 
                    onChange={(e) => handleInputChanges(e)} 
                    name="email" 
                    className="form-control h-100"  
                    placeholder="Company email" />
                  </div>
                </div>
                
                <button 
                className="btn btn-primary flex-shrink-0" 
                type="submit">
                  Create Post
                  {loading &&
                    <span className="fa fa-circle-o-notch fa-spin" />
                  }
                </button>
              </form>
              
            </div>
          </div>
        </div>
      </section>
    </div>
  );

}
export default withRouter(Create)
