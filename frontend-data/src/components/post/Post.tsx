import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom'
import { useParams } from 'react-router';
import { useAuth0 } from '../../contexts/auth0-context';

function Post() {
  let history = useHistory()
  const { isAuthenticated, getIdTokenClaims, user } = useAuth0();

  let { postId } = useParams();
  const [reports, setReports] = useState();
  const [post, setPost] = useState<any>({});

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/company/post/${postId}`);
      const json = await response.json();
      setPost(json);
    }
    fetchData();
  }, [postId]);

  useEffect(() => {
    const fetchReports = async (): Promise<any> => {
      const response = await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/report/reports`);
      const json = await response.json();
      setReports(json)
    }
    fetchReports();
  }, [])
  
  const deleteReport = async(id: string) => {
    const accessToken = await getIdTokenClaims();
    await fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/report/delete?reportID=${id}`, {
      method: "delete",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json",
        "authorization": `Bearer ${accessToken.__raw}`
      })
    });
    _removeReportFromView(id);
    history.push(`/post/${postId}`);
  }

  const _removeReportFromView = (id: string) => {
    const index = reports.findIndex((report: { _id: string; }) => report._id === id);
    reports.splice(index, 1);
  }

  return (
    <div>
      <div className="bg-gradient text-white o-hidden" data-overlay>
        <section className="pb-5 pt-5"> </section>
      </div>

      <section className="pt-6">
        <div className="container">
          <div className="row align-items-start justify-content-around">
            
            <div className="col-md-9 col-lg col-xl-6 sticky-lg-top mb-5 mb-lg-0">
              <div className="col-xl-12 col-lg-9 col-md-10">
                <div className="d-flex justify-content-between align-items-center mb-3 mb-xl-4 pb-3">
                  <h2 className="mb-0">Reports</h2>
                  {
                    isAuthenticated && (user.name === post.author) &&
                    <Link to={`/add-report/${post._id}`} className="btn btn-success btn-sm">Add Report</Link>
                  } 
                </div>

                <div className="px-3 d-none d-md-block">
                  <div className="row mb-4">
                    <div className="col-3">
                      <h5 className="mb-0">Title</h5>
                    </div>
                    <div className="col-2">
                      <h5 className="mb-0">Period/Year</h5>
                    </div>
                    <div className="col-3">
                      <h5 className="mb-0">Assignee</h5>
                    </div>
                    <div className="col-2">
                      <h5 className="mb-0">Deadline</h5>
                    </div>
                    <div className="col">
                      <h5 className="mb-0">Actions</h5>
                    </div>
                  </div>
                </div>
                
                {reports && reports.map((report: { title: React.ReactNode; _id: any; period: any; year: any; assignee: any; deadline: any; postId: any; author: any }) => (
                  ((report.postId === postId) &&
                    <div className="card card-body px-3 py-4 flex-sm-row align-items-center justify-content-between mb-2 mb-sm-3" key={report._id}>
                      <div className="col-auto col-md-3">
                        <h6 className="mb-0">
                          <Link to={`/report/${report._id}`}>{report.title}</Link>
                        </h6>
                      </div>
                      <div className="col-auto col-md-2">
                        {report.period}/{report.year}
                      </div>
                      <div className="col-auto col-md-3">
                        {report.assignee}
                      </div>
                      <div className="col-auto col-md-2">
                        {report.deadline}
                      </div>
                      <div className="col-auto col">
                        {
                          !isAuthenticated &&
                          <div className="">
                            Not SignedIn
                          </div>
                        }
                        {
                          isAuthenticated && (user.name === post.author) &&
                          <button className="btn btn-danger btn-sm" onClick={() => deleteReport(report._id)}>Delete</button>
                        }
                      </div>
                    </div>
                  )
                ))}
              </div>
            </div>

            <div className="col-xl-5 col-lg-8 col-md-9">
              <div>
                {post && 
                  <div className="d-flex flex-column flex-lg-row no-gutters border rounded bg-white o-hidden" key={post._id}>
                    <a href="/" className="d-block position-relative bg-gradient col-xl-4">
                      <img className="flex-fill hover-fade-out" src="/img/Home_cover.jpg" alt="" />
                      <div className="divider divider-side bg-white d-none d-lg-block"></div>
                    </a>

                    <div className="p-2 p-md-3 col-xl-7 d-flex align-items-center">
                      <div>
                        <div className="d-flex justify-content-between align-items-center mb-3 mb-xl-4">
                          {/* eslint-disable-next-line */}
                          <a href="" className="badge badge-pill badge-info">No. of Reports: {post.reportsCount | 0}</a>
                          <div className="text-small text-muted">{post.createdAt}</div>
                        </div>

                        <h1>{post.name}</h1>

                        <p className="lead">
                          {post.description}
                        </p>

                        <p className="lead">
                          {post.address} <br />
                          {post.email}
                        </p> <br />

                        <p className="lead">
                          Posted By: {post.author}
                        </p>

                        <Link to="/" className="lead">Return</Link>
                      </div>
                    </div>
                  </div>
                }
              </div>
            </div>
          </div>

        </div>
      </section>

    </div>
  );
}

export default Post;